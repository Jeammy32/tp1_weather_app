package ca.csf.mobile2.tp1.Data

enum class WeatherType{
    CLOUDY,
    PARTLY_SUNNY,
    RAIN,
    SNOW,
    SUNNY
}