package ca.csf.mobile2.tp1

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import ca.csf.mobile2.tp1.AsyncTask.WeatherAsynchtask
import ca.csf.mobile2.tp1.Data.WeatherData
import ca.csf.mobile2.tp1.Data.WeatherType
import kotlin.Exception


class MainActivity : AppCompatActivity() {
    private lateinit var searchTextBox: EditText
    private lateinit var weatherImage: ImageView
    private lateinit var weatherText: TextView
    private lateinit var city: TextView
    private lateinit var progressBar: androidx.core.widget.ContentLoadingProgressBar
    private lateinit var appContent: androidx.constraintlayout.widget.Group
    private lateinit var errorContent: androidx.constraintlayout.widget.Group

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        searchTextBox = findViewById(R.id.search_text_box)
        weatherImage = findViewById(R.id.image_switcher)
        weatherText = findViewById(R.id.weather_text)
        progressBar = findViewById(R.id.progress_bar)
        city = findViewById(R.id.city)
        appContent = findViewById(R.id.app_content)
        errorContent = findViewById(R.id.error_content)

        setGroupVisibility(ViewType.NOTHING)

        //https://stackoverflow.com/questions/47298935/handling-enter-key-on-edittext-kotlin-android
        searchTextBox.setOnEditorActionListener { _, actionId, _ ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    //fermer le clavier
                    if (!searchTextBox.text.isEmpty()){
                        val imm: InputMethodManager =
                            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(searchTextBox.getWindowToken(), 0)
                        //executer l'asynctask
                        WeatherAsynchtask(this::onSuccess,this::onError404,this::onErrorWrongRequest).execute(searchTextBox.text.toString())
                        progressBar.visibility = View.VISIBLE
                    }
                    true
                }
                else -> false
            }
        }
    }

    private fun onSuccess(weatherData: WeatherData) {
        progressBar.visibility = View.GONE
        setGroupVisibility(ViewType.CONTENT)
        setWeatherImage(weatherData)
        setCity(weatherData)
        setWeather(weatherData)
    }

    fun onError404() {
        setGroupVisibility(ViewType.ERROR)
    }

    fun onErrorWrongRequest (e : Exception){
        setGroupVisibility(ViewType.NOTHING)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("SEARCH_TEXT_BOX",searchTextBox.text.toString())
        outState.putInt("WEATHER_IMAGE",weatherImage.drawable.alpha)
        outState.putString("CITY",city.text.toString())
        outState.putString("WEATHER_TEXT",weatherText.text.toString())

        when (appContent.visibility){
            View.VISIBLE -> outState.putString("APP_CONTENT",ViewType.CONTENT.toString())
            View.GONE -> outState.putString("APP_CONTENT",ViewType.NOTHING.toString())
            else -> {
                outState.putString("APP_CONTENT",ViewType.NOTHING.toString())
            }
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        searchTextBox.setText(savedInstanceState.getString("SEARCH_TEXT_BOX"))
        city.setText(savedInstanceState.getString("CITY"))
        weatherText.setText(savedInstanceState.getString("WEATHER_TEXT"))

        val appContentVisibility = savedInstanceState.getString("APP_CONTENT")
        if (appContentVisibility != null){
            setGroupVisibility(ViewType.valueOf(appContentVisibility))
        }
    }

    fun setWeather(weatherData: WeatherData) {
        weatherText.text = "${weatherData.temperatureInCelsius} °C"
    }

    fun setCity(weatherData: WeatherData) {
        city.text = weatherData.city
    }

    fun setWeatherImage(weatherData: WeatherData) {
        when (WeatherType.valueOf(weatherData.type)) {
            WeatherType.CLOUDY -> weatherImage.setImageResource(R.drawable.ic_cloudy)
            WeatherType.PARTLY_SUNNY -> weatherImage.setImageResource(R.drawable.ic_partly_sunny)
            WeatherType.RAIN -> weatherImage.setImageResource(R.drawable.ic_rain)
            WeatherType.SNOW -> weatherImage.setImageResource(R.drawable.ic_snow)
            WeatherType.SUNNY -> weatherImage.setImageResource(R.drawable.ic_sunny)
        }
    }

    fun setGroupVisibility(viewType: ViewType) {
        when (viewType) {
            ViewType.CONTENT -> {
                appContent.visibility = View.VISIBLE
                errorContent.visibility = View.GONE
            }
            ViewType.ERROR -> {
                appContent.visibility = View.GONE
                appContent.visibility = View.VISIBLE
            }
            ViewType.NOTHING -> {
                appContent.visibility = View.GONE
                errorContent.visibility = View.GONE
            }
        }
    }
}

enum class ViewType {
    CONTENT,
    ERROR,
    NOTHING
}