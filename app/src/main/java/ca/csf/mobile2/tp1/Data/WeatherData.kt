package ca.csf.mobile2.tp1.Data

data class WeatherData(
    val city : String,
    val temperatureInCelsius : Int,
    val type : String)