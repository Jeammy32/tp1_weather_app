package ca.csf.mobile2.tp1.AsyncTask

import android.os.AsyncTask
import ca.csf.mobile2.tp1.Data.WeatherData
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import java.net.URL
import kotlin.Exception

class WeatherAsynchtask (var onSucces : (WeatherData) -> Unit , var onError404 : () -> Unit , var onErrorWrongRequest : (Exception) -> Unit): AsyncTask<String,Unit,WeatherData>() {

    private lateinit var weather : WeatherData
    private lateinit var response : Response

    override fun doInBackground(vararg params: String?): WeatherData {
        val url = URL("http://$IP_ADDRESS:8080/api/v1/weather/${params[0]}")
        var client = OkHttpClient()
        val request = Request.Builder()
            .url(url)
            .build()
        try {
            response = client.newCall(request).execute()
            if (response.isSuccessful){
                val mapper = jacksonObjectMapper().registerModule(KotlinModule())
                weather = mapper.readValue(response.body()?.string().toString())
                response.close()
            } else {
                //TODO: 404 error
                weather = WeatherData("quebec",0,"sunny")
                onError404()
                response.close()
            }
        } catch (e : IOException) {
            //TODO: no internet error
            onErrorWrongRequest(e)
            response.close()
        }
        return weather
    }

    override fun onPostExecute(result: WeatherData) {
        super.onPostExecute(result)
        onSucces(result)
    }
}

private const val IP_ADDRESS = "192.168.1.18"